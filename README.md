# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This app will estimate hormone levels over time using averaged lab results over time combined with individual user's lab results to generate a graph. The intent is for a person to be able to visualize the swings in their hormone levels and adjust their dose and cycle based on individual needs.

* Version (no current version)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner and admin: DeAnna Williams
* Other community or team contact